<!DOCTYPE html>
<html lang="en">
<head>
  <title>INFORMASI SMARTPHONE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <script src="jquery.js"></script>
</head>

<style>
  body {
      position: relative; 
  }
  #section1 {padding-top:5px;height:655px}
  #section2 {padding-top:20px;height:300px;color: #000; background-color: #DCDCDC;}
  #section3 {padding-top:10px;height:150px;}
  #section31 {padding-top:10px;height:250px;}
  #section4 {padding-top:60px;height:610px;color: #fff; background-color: #000000;}
  #section41 {padding-top:50px;height:550px;color: #fff; background-color: #000000;}
  #section5 {padding-top:50px;height:500px;color: #000; background-color: #87CEFA;}
  </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<?php
   include('connect.php');
   error_reporting(E_ALL ^ E_NOTICE);

   if (isset($_GET['ID'])) {
       $ID = mysqli_real_escape_string($connect, $_GET['ID']);

       $sql = " SELECT a.Nama_series, a.Gambar, a.luncur, a.Harga, a.Spesifikasi_satu, 
       a.Spesifikasi_dua, a.Spesifikasi_tiga, a.Spesifikasi_empat, a.Spesifikasi_lima, a.Spesifikasi_enam, 
       a.Spesifikasi_tujuh, a.Spesifikasi_delapan, a.Spesifikasi_delapan, a.Spesifikasi_sembilan, 
       a.Spesifikasi_sepuluh, a.Kelebihan_satu, a.Kelebihan_dua, a.Kelebihan_tiga, a.Kelebihan_empat, 
       a.Kelebihan_lima, a.Kekurangan, a.embed, c.lat, c.lng, c.name FROM tbl_series a, markers c WHERE Kode_hp = '$ID' 
       AND c.id = a.id";

       $result = mysqli_query($connect, $sql);
       $ayam = mysqli_fetch_array($result);

       echo '<div id="section1" class="container-fluid">';
       echo '<center>';
       echo '<h4 class="display-4">' .$ayam['Nama_series']. '</h4></center>'; 
       echo'<br>';
       echo '<div class="row">';
       echo '<div class="col">';
       echo '<center>';
       echo '<img src="data:image/jpeg;base64,'.base64_encode( $ayam['Gambar'] ).'"/>';
       echo '<br><br>';
       
       echo '<h5> <b> RELEASE : ' .$ayam['luncur'].  '</h5> </b><br>';
       echo '<h5> <b> HARGA : ' .$ayam['Harga'].   '</h5> </b>';
       echo '</center>';
       echo '</div>';
       echo '<div class="col">';
       echo '<embed width="600" height="420" src="https://www.youtube.com/v/'.$ayam['embed']. '">';
       echo '</div>';
       echo '</div>';
       echo '</div>';
       
       echo '<div id="section2" class="container-fluid">';
       echo '<p class="font-weight-bold h3 mb-2 text-center">NETWORK <br><br></p>';
       echo '<div class="row">';
       echo '<div class="col-4">';
       echo '<center>';
       echo '<img src = "gambar/baseline-sim_card-24px.svg" height="135" width="150"> </center> </div>';
       echo'<div class="col-8">';
       echo '<article>';
       echo '<p class =" ml-5">' .$ayam['Spesifikasi_satu']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';
       echo '</div>';

       echo '<br><br><br>';
       echo '<div id="section3" class="container-fluid">';
       echo '<p class="font-weight-bold h3 mb-2 text-center"><u>BATERAI<br><br></u></p>';
       echo '<article>';
       echo '<h4 class ="text-center">' .$ayam['Spesifikasi_sepuluh']. '</h4>';
       echo '</article>';
       echo '</div>';

       echo '<br><br>';
       echo '<div id="section4" class="container-fluid">';
       echo '<div class="row">';
       echo '<div class="col-2">';
       echo '<center>';
       echo '<p class="font-weight-bold h3 text-center">BODY</p></center> </div>';
       echo'<div class="col-8">';
       echo '<article>';
       echo '<p>' .$ayam['Spesifikasi_dua']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';
       echo '<br><br>';

       echo '<div class="row">';
       echo '<div class="col-4">';
       echo '<center>';
       echo '<p class="font-weight-bold h3 text-center">DISPLAY</p></center></div>';
       echo'<div class="col-7">';
       echo '<article>';
       echo '<p>' .$ayam['Spesifikasi_tiga']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';
       echo '<br><br>';

       echo '<div class="row">';
       echo '<div class="col-6">';
       echo '<center>';
       echo '<p class="font-weight-bold h3 text-center">PLATFORM</p></center></div>';
       echo'<div class="col-6">';
       echo '<article>';
       echo '<p>' .$ayam['Spesifikasi_empat']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';

       echo '<br><br>';
       echo '<div class="row">';
       echo '<div class="col-8">';
       echo '<center>';
       echo '<p class="font-weight-bold h3 text-center">MEMORY</p></center></div>';
       echo'<div class="col-4">';
       echo '<article>';
       echo '<p>' .$ayam['Spesifikasi_lima']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';
       echo '</div>';


       echo '<br><br><br>';
       echo '<div id="section31" class="container-fluid">';
       echo '<p class="font-weight-bold h3 mb-2 text-center"><u>FEATURES<br><br></u></p>';
       echo '<article>';
       echo '<p class ="text-center">' .$ayam['Spesifikasi_sembilan']. '</p>';
       echo '</article>';
       echo '</div>';

       echo '<br><br><br>';
       echo '<div id="section5" class="container-fluid">';
       echo '<div class="row">';
       echo '<div class="col-2">';
       echo '<center>';
       echo '<img src = "gambar/baseline-camera-24px.svg" height="135" width="150"> </center> </div>';
       echo'<div class="col-2">';
       echo '<center>';
       echo '<p class="font-weight-bold h3 text-left"><br>CAMERA</p></center></div>';
       echo'<div class="col-7">';
       echo '<article>';
       echo '<p>' .$ayam['Spesifikasi_enam']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';
    
       echo '<br><br><br>';
       echo '<div class="row">';
       echo '<div class="col-2">';
       echo '<center>';
       echo '<img src = "gambar/baseline-volume_up-24px.svg" height="135" width="150"> </center> </div>';
       echo'<div class="col-2">';
       echo '<center>';
       echo '<p class="font-weight-bold h3 text-left"><br>SOUND</p></center></div>';
       echo'<div class="col-7">';
       echo '<article><br><br>';
       echo '<p>' .$ayam['Spesifikasi_tujuh']. '</p>';
       echo '</article>';
       echo '</div>';
       echo '</div>';
       echo '</div>';

       echo '<br><br><br>';
       echo '<div id="section31" class="container-fluid">';
       echo '<p class="font-weight-bold h3 mb-2 text-center"><u>COMMS<br><br></u></p>';
       echo '<article>';
       echo '<p class ="text-center">' .$ayam['Spesifikasi_delapan']. '</p>';
       echo '</article>';
       echo '</div>';

       echo '<div id="section41" class="container-fluid">';
       echo '<p class="font-weight-bold h3 mb-2 text-center">Kelebihan ' .$ayam['Nama_series']. '?<br><br></p>';
       echo '<br>';
       echo '<article> <p> <ul>';
       echo '<li>' .$ayam['Kelebihan_satu']. '</li><br>';
       echo '<li>' .$ayam['Kelebihan_dua']. '</li><br>';
       echo '<li>' .$ayam['Kelebihan_tiga']. '</li><br>';
       echo '<li>' .$ayam['Kelebihan_empat']. '</li><br>';
       echo'<li>' .$ayam['Kelebihan_lima']. '</li><br>';
       echo '</div>';
       echo '<br><br><br><br>';
       echo '<div class="container-fluid">';
       echo '<p class="font-weight-bold h3 mb-2 text-center">Kekurangan ' .$ayam['Nama_series']. '?<br><br></p>';
       echo '<article>';
       echo '<h5 class ="text-center">' .$ayam['Kekurangan']. '</h5>';
       echo '</article>';
       echo '</div>';
       echo '<br><br><br><br>';

       echo '<p class="font-weight-bold h3 mb-1 ml-5">Kantor Pusat<br><br></p>';
       echo '<div id="map" style="width:95%;height:450px" class = "ml-5"></div>';

    echo '<script>
        function myMap() {
            var mapCanvas = document.getElementById("map");
            var myCenter = new google.maps.LatLng(' .$ayam['lat']. ',' .$ayam['lng']. ');
            var mapOptions = { center: myCenter, zoom: 15
             };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: myCenter,
                animation: google.maps.Animation.BOUNCE
            });
            marker.setMap(map);
            google.maps.event.addListener(marker,"click",function() {
                var infowindow = new google.maps.InfoWindow({
                  content:"'.$ayam['name']. '"
                });
              infowindow.open(map,marker);
              });
        }
    </script>';

    echo '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuQjW7zFaIqdEhjKrAqvlMmX8JSNaNzQg&callback=myMap"></script>';
   }
       ?>
       <br><br><br>
       <div class = "ml-5">
       <a href = "index.php"class="btn btn-info btn-lg">
          <span class="glyphicon glyphicon-backward ml-7"></span> KEMBALI
        </a>
        </div>
       </body>
       </html>